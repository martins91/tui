import { omit } from "lodash";
import { Passenger, PassengersMap } from "../Pages/App/types";

export const mapToPassengersArray = (passengersMap: PassengersMap): Passenger[] => {
  return Object.entries(passengersMap).map(([id, passengerObj]) => {
    return { id: parseInt(id), dob: passengerObj["date-of-birth"], ...omit(passengerObj, "date-of-birth") };
  });
};
