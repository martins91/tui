import { isEmpty } from "lodash";
import { PassengerObject } from "../Pages/App";

export const isDataPassengersMap = (data: any): boolean => {
  if (typeof data !== "object" || data === null || isEmpty(data)) return false;
  for (const key in data) {
    if (typeof key !== "string" || typeof data[key] !== "object" || data[key] === null) {
      return false;
    }
    const passenger = data[key] as PassengerObject;
    const extraFields = Object.keys(passenger).filter(
      (field) => !["title", "gender", "firstName", "lastName", "date-of-birth"].includes(field)
    );
    if (extraFields.length > 0) {
      return false;
    }
    if (
      typeof passenger.title !== "string" ||
      typeof passenger.gender !== "string" ||
      typeof passenger.firstName !== "string" ||
      typeof passenger.lastName !== "string" ||
      typeof passenger["date-of-birth"] !== "string"
    ) {
      return false;
    } else if (
      !isTitleValid(passenger.title) ||
      !isGenderValid(passenger.gender) ||
      !isDateValid(passenger["date-of-birth"])
    ) {
      return false;
    }
  }

  return true;
};

export const isTitleValid = (title: string): boolean => {
  return title === "MRS" || title === "MR";
};

export const isGenderValid = (gender: string): boolean => {
  return gender === "MALE" || gender === "FEMALE";
};

export const isDateValid = (date: string): boolean => {
  return !isNaN(Date.parse(date));
};
