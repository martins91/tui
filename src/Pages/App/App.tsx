import React from "react";
import PassengersManager from "../../Components/PassengersManager/PassengersManager";
import { mapToPassengersArray } from "../../utils";
import { ConfirmationWrapper, Wrapper } from "./styles";
import { Passenger, PassengersMap } from "./types";

const initialPassengers: PassengersMap = {
  "0": {
    title: "MRS",
    gender: "FEMALE",
    firstName: "Jane",
    lastName: "Doe",
    "date-of-birth": "2003-08-31",
  },
  "1": {
    title: "MR",
    gender: "MALE",
    firstName: "John",
    lastName: "De",
    "date-of-birth": "2001-04-12",
  },
};

function App() {
  const [passengers] = React.useState<Passenger[]>(mapToPassengersArray(initialPassengers));
  const [saved, setSaved] = React.useState(false);

  return (
    <Wrapper>
      {saved ? (
        <ConfirmationWrapper>Changes Saved</ConfirmationWrapper>
      ) : (
        <PassengersManager passengers={passengers} handleSave={() => setSaved(true)} />
      )}
    </Wrapper>
  );
}

export default App;
