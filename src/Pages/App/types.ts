export type PassengerObject = {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  "date-of-birth": string;
};

export type PassengersMap = Record<string, PassengerObject>;

export type Passenger = {
  id: number;
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  dob: string;
};
