import { styled } from "@mui/system";

export const Wrapper = styled("div")({
  display: "grid",
  minHeight: "100%",
  width: "100%",
});

export const ConfirmationWrapper = styled("div")(({ theme }) => ({
  margin: "auto",
  width: "90%",
  backgroundColor: "hsl(122, 40%, 50%)",
  color: "white",
  textAlign: "center",
  padding: "20px",
  borderRadius: "5px",
  boxShadow: "0px 0px 10px 0px hsla(0, 0%, 0%, 0.5)",
  [theme.breakpoints.up("md")]: {
    width: "40%",
  },
  [theme.breakpoints.up("lg")]: {
    width: "20%",
  },
}));
