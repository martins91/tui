import { Passenger, PassengersMap } from "../../Pages/App";
import { mapToPassengersArray } from "../../utils";

describe("mapToPassengersArray", () => {
  test("Convert a passengers map to an array of passengers", () => {
    const passengersMap: PassengersMap = {
      "1": { title: "Mr", gender: "Male", firstName: "John", lastName: "Doe", "date-of-birth": "2003-08-31" },
      "2": { title: "Mrs", gender: "Female", firstName: "Jane", lastName: "Doe", "date-of-birth": "2001-04-12" },
    };
    const expected: Passenger[] = [
      { id: 1, title: "Mr", gender: "Male", firstName: "John", lastName: "Doe", dob: "2003-08-31" },
      { id: 2, title: "Mrs", gender: "Female", firstName: "Jane", lastName: "Doe", dob: "2001-04-12" },
    ];
    const result = mapToPassengersArray(passengersMap);
    expect(result[0]).toEqual(expected[0]);
    expect(result[1]).toEqual(expected[1]);
  });
});
