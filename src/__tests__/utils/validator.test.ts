import { isDataPassengersMap, isDateValid, isGenderValid, isTitleValid } from "../../utils";

describe("isDataPassengersMap", () => {
  test("Return false for invalid data", () => {
    expect(isDataPassengersMap(null)).toBe(false);
    expect(isDataPassengersMap({})).toBe(false);
    expect(isDataPassengersMap({ x: 1 })).toBe(false);
  });

  test("Return false for invalid type fields", () => {
    const data = [{ title: "MRS", gender: "FEMALE", firstName: 1, lastName: "Doe", "date-of-birth": "2003-08-31" }];
    expect(isDataPassengersMap(data)).toBe(false);
  });

  test("Return false for extra data fields", () => {
    const data = [
      { title: "MRS", extra: 1, gender: "FEMALE", firstName: "Jane", lastName: "Doe", "date-of-birth": "2003-08-31" },
    ];
    expect(isDataPassengersMap(data)).toBe(false);
  });

  test("Return true for valid data", () => {
    const data = [
      { title: "MRS", gender: "FEMALE", firstName: "Jane", lastName: "Doe", "date-of-birth": "2003-08-31" },
      { title: "MR", gender: "MALE", firstName: "John", lastName: "Doe", "date-of-birth": "2001-04-12" },
    ];
    expect(isDataPassengersMap(data)).toBe(true);
  });

  test("Return false for invalid title", () => {
    const data = [
      { title: "MISS", gender: "FEMALE", firstName: "Jane", lastName: "Doe", "date-of-birth": "2003-08-31" },
    ];
    expect(isDataPassengersMap(data)).toBe(false);
  });

  test("Return false for invalid gender", () => {
    const data = [{ title: "MR", gender: "OTHER", firstName: "John", lastName: "Doe", "date-of-birth": "2001-04-12" }];
    expect(isDataPassengersMap(data)).toBe(false);
  });
});

describe("isTitleValid", () => {
  test("Return true for valid titles", () => {
    expect(isTitleValid("MR")).toBe(true);
    expect(isTitleValid("MRS")).toBe(true);
  });

  test("Return false for invalid titles", () => {
    expect(isTitleValid("")).toBe(false);
    expect(isTitleValid("Mr")).toBe(false);
    expect(isTitleValid("mrs")).toBe(false);
  });
});

describe("isGenderValid", () => {
  test("Return true for valid genders", () => {
    expect(isGenderValid("MALE")).toBe(true);
    expect(isGenderValid("FEMALE")).toBe(true);
  });

  test("Return false for invalid genders", () => {
    expect(isGenderValid("")).toBe(false);
    expect(isGenderValid("Ma")).toBe(false);
    expect(isGenderValid("female")).toBe(false);
  });
});

describe("isDateValid", () => {
  test("Return true for valid date", () => {
    expect(isDateValid("2003-08-31")).toBe(true);
    expect(isDateValid("2001-04-12")).toBe(true);
  });

  test("Return false for invalid dates", () => {
    expect(isDateValid("")).toBe(false);
    expect(isDateValid("date")).toBe(false);
  });
});
