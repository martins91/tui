import React from "react";
import { render, screen } from "@testing-library/react";
import Confirmation from "../../../Components/Confirmation/Confirmation";

describe("Confirmation component", () => {
  const initialPassengers = [
    { id: 0, title: "MRS", gender: "FEMALE", firstName: "Jane", lastName: "Doe", dob: "2003-08-31" },
    { id: 1, title: "MR", gender: "MALE", firstName: "John", lastName: "Do", dob: "2001-04-12" },
  ];
  const passengers = [
    { id: 0, title: "MRS", gender: "FEMALE", firstName: "Janet", lastName: "Doe", dob: "2003-08-31" },
    { id: 1, title: "MR", gender: "MALE", firstName: "John", lastName: "Do", dob: "2001-04-12" },
  ];

  test("Show the updated value with a different class", () => {
    render(<Confirmation initialPassengers={initialPassengers} passengers={passengers} />);
    const firstName = screen.getByText(/janet/i);
    const lastName = screen.getByText(/doe/i);
    const firstNameJohn = screen.getByText(/john/i);
    
    const firstNameAttributes = firstName.getAttribute('class');
    const lastNameAttributes = lastName.getAttribute('class');
    const firstNameJohnAttributes = firstNameJohn.getAttribute('class');

    expect(lastNameAttributes).toEqual(firstNameJohnAttributes)
    expect(firstNameAttributes).not.toEqual(firstNameJohnAttributes)
  });
});
