import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import LoadPassengers from "../../../Components/LoadPassengers/LoadPassengers";

describe("LoadPassengers component", () => {
  test("Display error message for invalid JSON data", async () => {
    render(<LoadPassengers open={true} onClose={() => {}} onSave={() => {}} />);
    const jsonInput = screen.getByLabelText(/paste JSON data/i);
    
    fireEvent.change(jsonInput, { target: { value: "invalid json" } });
    fireEvent.click(screen.getByText("Save"));

    await waitFor(() => {
      const errorMessage = screen.getByText(/invalid JSON data/i);
      expect(errorMessage).toBeInTheDocument();
    });
  });

  test("Display error message for invalid object data", async () => {
    render(<LoadPassengers open={true} onClose={() => {}} onSave={() => {}} />);
    const jsonInput = screen.getByLabelText(/paste JSON data/i);
    const data ={'0':{title:'MISS',gender:'FEMALE',firstName:'Jane',lastName:'Doe','date-of-birth':'2003-08-31' }};
    fireEvent.change(jsonInput, { target: { value: JSON.stringify(data) } });
    fireEvent.click(screen.getByText(/save/i));
    await waitFor(() => {
      const errorMessage = screen.getByText(/Invalid Object data/i);
      expect(errorMessage).toBeInTheDocument();
    });
  });
  
  test("Call onSave with parsed data and close modal", () => {
    const mockOnSave = jest.fn();
    const mockOnClose = jest.fn();
    render(<LoadPassengers open={true} onClose={mockOnClose} onSave={mockOnSave} />);
    const jsonInput = screen.getByLabelText("Paste JSON Data");
    const data = '[{"title": "MRS", "gender": "FEMALE", "firstName": "Jane", "lastName": "Doe", "date-of-birth": "2003-08-31"}]';
    fireEvent.change(jsonInput, {target: {value: data }});
    fireEvent.click(screen.getByText("Save"));

    expect(mockOnSave).toHaveBeenCalledWith([
      { title: "MRS", gender: "FEMALE", firstName: "Jane", lastName: "Doe", "date-of-birth": "2003-08-31" },
    ]);
    expect(mockOnClose).toHaveBeenCalled();
  });
});
