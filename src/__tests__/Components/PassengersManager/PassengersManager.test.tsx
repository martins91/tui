import React from "react";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import PassengersManager from "../../../Components/PassengersManager/PassengersManager";

describe("PassengersManager component", () => {
  const initialPassengers = [
    { id: 0, title: "MRS", firstName: "Jane", lastName: "Doe", gender: "FEMALE", dob: "2003-08-31" },
  ];

  const mockHandleSave = jest.fn();

  test("Show passenger details accordion", () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    expect(screen.getByText(/^passenger details$/i)).toBeInTheDocument();
  });

  test("Submits changes when Save button is clicked", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const input = screen.getByLabelText(/first name/i);
    fireEvent.change(input, { target: { value: "Janet" } });
    const saveChangesButton = screen.getByText(/save changes/i);
    fireEvent.click(saveChangesButton);
    await waitFor(() => {
      const confirm = screen.getByText(/confirm changes/i);
      expect(confirm).toBeInTheDocument();
    });
    const saveButton = screen.getByText(/save/i);
    fireEvent.click(saveButton);
    expect(mockHandleSave).toHaveBeenCalled();
  });

  test("Save button is disabled", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const input = screen.getByLabelText(/first name/i);
    fireEvent.change(input, { target: { value: "disabled" } });
    const saveChangesButton = screen.getByText(/save changes/i);
    expect(saveChangesButton).toBeDisabled();
  });

  test("Save button is enabled after being disabled", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const input = screen.getByLabelText(/first name/i);
    fireEvent.change(input, { target: { value: "Disabled" } });
    const saveChangesButton = screen.getByText(/save changes/i);
    expect(saveChangesButton).toBeDisabled();
    fireEvent.change(input, { target: { value: "Janet" } });
    expect(saveChangesButton).toBeEnabled();
  });

  test("Goes back to edit changes when Back button is clicked", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const input = screen.getByLabelText(/first name/i);
    expect(input).toHaveValue("Jane");
    fireEvent.change(input, { target: { value: "Janet" } });
    const saveChangesButton = screen.getByText(/save changes/i);
    fireEvent.click(saveChangesButton);
    await waitFor(() => {
      const confirm = screen.getByText(/confirm changes/i);
      expect(confirm).toBeInTheDocument();
    });
    const backButton = screen.getByText(/back/i);
    fireEvent.click(backButton);

    await waitFor(() => {
      const details = screen.getByText(/^passenger details$/i);
      expect(details).toBeInTheDocument();
    });

    expect(input).toHaveValue("Janet");
  });

  test("Don't allow empty field", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const input = screen.getByLabelText(/last name/i);
    fireEvent.change(input, { target: { value: "" } });
    const saveChangesButton = screen.getByText(/save changes/i);
    fireEvent.click(saveChangesButton);
    await waitFor(() => {
      expect(input).toBeInvalid();
    });
  });

  test("Load data from object to Passenger Details Form", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const loadButton = screen.getByText(/load passenger details/i);
    fireEvent.click(loadButton);
    await waitFor(() => {
      expect(screen.getByLabelText(/paste JSON data/i)).toBeInTheDocument();
    });
    const jsonInput = screen.getByLabelText(/paste JSON data/i);
    const data =
      '{"0":{"title": "MR", "gender": "MALE", "firstName": "Gonçalo", "lastName": "Martins", "date-of-birth": "1991-01-26"}}';
    fireEvent.change(jsonInput, { target: { value: data } });
    fireEvent.click(screen.getByText("Save"));

    await waitFor(() => {
      expect(jsonInput).not.toBeInTheDocument();
    });

    expect(screen.getByLabelText(/last name/i)).toHaveValue("Martins");
    expect(screen.getByLabelText(/date of birth/i)).toHaveValue("1991-01-26");
  });

  test("Cancel on load and not change data of Passenger Details Form", async () => {
    render(<PassengersManager passengers={initialPassengers} handleSave={mockHandleSave} />);
    const loadButton = screen.getByText(/load passenger details/i);
    fireEvent.click(loadButton);
    await waitFor(() => {
      expect(screen.getByLabelText(/paste JSON data/i)).toBeInTheDocument();
    });
    const jsonInput = screen.getByLabelText(/paste JSON data/i);
    const data =
      '{"0":{"title": "MR", "gender": "MALE", "firstName": "Gonçalo", "lastName": "Martins", "date-of-birth": "1991-01-26"}}';
    fireEvent.change(jsonInput, { target: { value: data } });
    fireEvent.click(screen.getByText("Cancel"));

    await waitFor(() => {
      expect(jsonInput).not.toBeInTheDocument();
    });

    expect(screen.getByLabelText(/last name/i)).toHaveValue("Doe");
    expect(screen.getByLabelText(/date of birth/i)).toHaveValue("2003-08-31");
  });
});
