import { styled } from "@mui/system";
import { Button } from "@mui/material";

export const PrimaryButtonSt = styled(Button)({
  backgroundColor: "hsl(248, 69%, 21%)",
  color: "hsl(198, 87%, 94%)",
  textTransform: "uppercase",
  border: "1px solid hsl(248, 69%, 21%)",

  ":hover": {
    backgroundColor: "hsl(248, 69%, 42%)",
  },

  ":disabled": {
    backgroundColor: "hsl(248, 5%, 21%)",
    color: "hsl(198, 87%, 94%)",
  },
});

export const SecondaryButtonSt = styled(Button)({
  color: "hsl(248, 69%, 21%)",
  backgroundColor: "hsl(0, 0%, 100%)",
  textTransform: "uppercase",
  border: "1px solid hsl(198, 10%, 67%)",

  ":hover": {
    backgroundColor: "hsl(198, 10%, 67%)",
  },
});

export const CancelButtonSt = styled(Button)({
  color: "hsl(0, 65%, 51%)",
  backgroundColor: "hsl(0, 0%, 100%)",
  textTransform: "uppercase",
  border: "1px solid hsl(0, 65%, 51%)",

  ":hover": {
    backgroundColor: "hsla(0, 65%, 51%, 0.05);",
  },
});
