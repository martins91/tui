export interface ButtonProps {
  buttonType?: "button" | "submit" | "reset";
  children: string;
  isDisabled?: boolean;
  onClick?: () => void;
}
