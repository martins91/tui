import { PrimaryButtonSt } from "./styles";
import { ButtonProps } from "./types";

export const PrimaryButton: React.FC<ButtonProps> = ({
  children,
  buttonType = "button",
  isDisabled = false,
  onClick,
}) => {
  return (
    <PrimaryButtonSt type={buttonType} onClick={onClick} disabled={isDisabled}>
      {children}
    </PrimaryButtonSt>
  );
};
