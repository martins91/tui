import { SecondaryButtonSt } from "./styles";
import { ButtonProps } from "./types";

export const SecondaryButton: React.FC<ButtonProps> = ({
  children,
  buttonType = "button",
  isDisabled = false,
  onClick,
}) => {
  return (
    <SecondaryButtonSt type={buttonType} onClick={onClick} disabled={isDisabled}>
      {children}
    </SecondaryButtonSt>
  );
};
