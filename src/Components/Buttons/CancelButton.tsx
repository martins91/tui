import { CancelButtonSt } from "./styles";
import { ButtonProps } from "./types";

export const CancelButton: React.FC<ButtonProps> = ({
  children,
  buttonType = "button",
  isDisabled = false,
  onClick,
}) => {
  return (
    <CancelButtonSt type={buttonType} onClick={onClick} disabled={isDisabled}>
      {children}
    </CancelButtonSt>
  );
};
