import React, { useState } from "react";
import { Modal, TextField } from "@mui/material";
import { StyledBox, StyledDiv } from "./styles";
import { CancelButton, PrimaryButton } from "../Buttons";
import { LoadPassengerDetailsProps } from "./types";
import { isDataPassengersMap } from "../../utils";

const LoadPassengers: React.FC<LoadPassengerDetailsProps> = ({ open, onClose, onSave }) => {
  const [jsonData, setJsonData] = useState("");
  const [errorMessage, setErrorMessage] = useState<string | undefined>();

  const handleSave = () => {
    try {
      const parsedData = JSON.parse(jsonData);
      if (!isDataPassengersMap(parsedData)) {
        setErrorMessage("Invalid Object data");
      } else {
        onSave(parsedData);
        setJsonData("");
        setErrorMessage(undefined);
        onClose();
      }
    } catch (error) {
      setErrorMessage("Invalid JSON data");
    }
  };

  const handleClose = () => {
    setJsonData("");
    setErrorMessage(undefined);
    onClose();
  };

  return (
    <Modal open={open} onClose={handleClose}>
      <StyledBox>
        <TextField
          multiline
          rows={10}
          fullWidth
          value={jsonData}
          onChange={(e) => setJsonData(e.target.value)}
          label="Paste JSON Data"
          error={errorMessage ? true : false}
          helperText={errorMessage}
        />
        <StyledDiv>
          <CancelButton onClick={handleClose}>Cancel</CancelButton>
          <PrimaryButton onClick={handleSave}>Save</PrimaryButton>
        </StyledDiv>
      </StyledBox>
    </Modal>
  );
};

export default LoadPassengers;
