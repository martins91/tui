import { PassengersMap } from "../../Pages/App";

export interface LoadPassengerDetailsProps {
  open: boolean;
  onClose: () => void;
  onSave: (passengerDetails: PassengersMap) => void;
}
