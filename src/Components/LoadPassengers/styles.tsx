import { Box } from "@mui/material";
import { styled } from "@mui/system";

export const StyledBox = styled(Box)(({ theme }) => ({
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  boxShadow: "24",
  backgroundColor: "hsl(180,0%,100%)",
  padding: "32px",
  width: "90%",
  [theme.breakpoints.up("md")]: {
    width: "70%",
  },
  [theme.breakpoints.up("lg")]: {
    width: "60%",
  },
  [theme.breakpoints.up("xl")]: {
    width: "45%",
  },
}));

export const StyledDiv = styled("div")({
  display: "flex",
  justifyContent: "space-between",
  marginTop: "16px",
});
