import React, { useEffect } from "react";
import { TextField, MenuItem, Grid } from "@mui/material";
import { StyledSubTitle, StyledWrapper } from "./styles";
import { Passenger } from "../../Pages/App";
import { Controller } from "react-hook-form";
import { PassengerDetailsProps } from "./types";

const PassengerDetails: React.FC<PassengerDetailsProps> = ({ index, passenger, control, maxChanges, setDisabled }) => {
  const [errorFirstName, setErrorFirstName] = React.useState<boolean>(false);
  const [errorLastName, setErrorLastName] = React.useState<boolean>(false);

  const aboveMaxChangesAllowed = (name: string, originalName: string): boolean => {
    const nameLength = name.length;
    const originalNameLength = originalName.length;
    const minLength = Math.min(nameLength, originalNameLength);
    const maxLength = Math.max(nameLength, originalNameLength);

    let count = 0;
    for (let i = 0; i < minLength; i++) {
      if (originalName[i] !== name[i]) {
        count++;
        if (count > maxChanges) {
          return true;
        }
      }
    }
    count += maxLength - minLength;

    return count > maxChanges;
  };

  const handleChange = (field: keyof Passenger, value: string) => {
    if (field === "firstName") {
      setErrorFirstName(aboveMaxChangesAllowed(value, passenger.firstName));
    }
    if (field === "lastName") {
      setErrorLastName(aboveMaxChangesAllowed(value, passenger.lastName));
    }
  };

  useEffect(() => {
    setDisabled(errorFirstName || errorLastName);
  }, [errorFirstName, errorLastName, setDisabled]);

  return (
    <StyledWrapper>
      <StyledSubTitle>Edit Passenger Details</StyledSubTitle>
      <Grid container spacing={2} style={{ marginBottom: "8px" }}>
        <Grid item xs={12} sm={3}>
          <Controller
            name={`passengerArray.${index}.title`}
            control={control}
            render={({ field }) => (
              <TextField {...field} required label="Title" select fullWidth variant="standard">
                <MenuItem value="MR">Mr</MenuItem>
                <MenuItem value="MRS">Mrs</MenuItem>
              </TextField>
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Controller
            name={`passengerArray.${index}.firstName`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                required
                error={errorFirstName}
                variant="standard"
                label="First Name"
                fullWidth
                onChange={(e) => {
                  handleChange("firstName", e.target.value);
                  field.onChange(e);
                }}
                helperText={errorFirstName ? "Exceeded maximum allowed changes." : null}
              />
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Controller
            name={`passengerArray.${index}.lastName`}
            control={control}
            render={({ field }) => (
              <TextField
                {...field}
                required
                error={errorLastName}
                variant="standard"
                label="Last Name"
                onChange={(e) => {
                  handleChange("lastName", e.target.value);
                  field.onChange(e);
                }}
                helperText={errorLastName ? "Exceeded maximum allowed changes." : null}
                fullWidth
              />
            )}
          />
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={3}>
          <Controller
            name={`passengerArray.${index}.gender`}
            control={control}
            render={({ field }) => (
              <TextField required {...field} label="Gender" select fullWidth variant="standard">
                <MenuItem value="MALE">Male</MenuItem>
                <MenuItem value="FEMALE">Female</MenuItem>
              </TextField>
            )}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Controller
            name={`passengerArray.${index}.dob`}
            control={control}
            render={({ field }) => (
              <TextField
                required
                {...field}
                label="Date of Birth"
                type="date"
                variant="standard"
                fullWidth
                InputLabelProps={{ shrink: true }}
              />
            )}
          />
        </Grid>
      </Grid>
    </StyledWrapper>
  );
};

export default PassengerDetails;
