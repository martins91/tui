import { TextField, Select } from "@mui/material";
import { styled } from "@mui/system";

export const StyledTextField = styled(TextField)({ marginRight: "8px" });

export const StyledSelect = styled(Select)({ marginRight: "8px" });

export const StyledSubTitle = styled("p")({
  color: "hsl(247, 35%, 30%)",
  textAlign: "left",
  marginBottom: "16px",
});

export const StyledWrapper = styled("div")({
  backgroundColor: "hsl(180,0%,100%)",
  padding: "8px",
  marginBottom: "8px",
  width: "100%",
  borderRadius: "5px",
  boxShadow: "0px 0px 10px 0px hsla(0, 0%, 0%, 0.5)",
});
