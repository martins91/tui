import { Control } from "react-hook-form";
import { Passenger } from "../../Pages/App/types";
import { PassengersFormData } from "../PassengersManager/types";

export interface PassengerDetailsProps {
  index: number;
  passenger: Passenger;
  control: Control<PassengersFormData>;
  maxChanges: number;
  setDisabled: (disabled: boolean) => void;
}
