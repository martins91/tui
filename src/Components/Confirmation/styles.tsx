import { Typography } from "@mui/material";
import { styled } from "@mui/system";
import { StyledSpanProps } from "./types";

export const ConfirmationWrapper = styled("div")({
  backgroundColor: "hsl(180,0%,100%)",
  padding: "8px",
  marginBottom: "8px",
  width: "100%",
});

export const StyledSpan = styled("span")<StyledSpanProps>((props) => ({
  color: props.updated ? " hsl(122, 40%, 50%)" : "hsl(0, 0%, 0%)",
}));

export const StyledCapitalizeSpan = styled(StyledSpan)({
  textTransform: "capitalize",
});

export const StyledTypography = styled(Typography)({
  color: "hsl(0, 0%, 50%)",
});

export const StyledSubTitle = styled("p")({
  color: "hsl(247, 35%, 30%)",
  textAlign: "left",
  marginBottom: "16px",
});

export const StyledWrapper = styled("div")({
  backgroundColor: "hsl(180,0%,100%)",
  padding: "8px",
  marginBottom: "8px",
  width: "100%",
  borderRadius: "5px",
  boxShadow: "0px 0px 10px 0px hsla(0, 0%, 0%, 0.5)",
});
