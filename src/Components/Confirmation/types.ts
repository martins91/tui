import { Passenger } from "../../Pages/App/types";

export interface StyledSpanProps {
  updated?: string;
}

export interface ConfirmationProps {
  initialPassengers: Passenger[];
  passengers: Passenger[];
}
