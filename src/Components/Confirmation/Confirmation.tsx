import { Grid } from "@mui/material";
import React from "react";
import {
  ConfirmationWrapper,
  StyledCapitalizeSpan,
  StyledSpan,
  StyledSubTitle,
  StyledTypography,
  StyledWrapper,
} from "./styles";
import { ConfirmationProps } from "./types";

const Confirmation: React.FC<ConfirmationProps> = ({ initialPassengers, passengers }) => {
  return (
    <StyledWrapper>
      <StyledSubTitle>Confirm Changes</StyledSubTitle>
      {Object.entries(passengers).map(([st, passenger], index) => (
        <ConfirmationWrapper key={st}>
          <Grid container spacing={2} style={{ marginBottom: "8px" }}>
            <Grid item xs={12} sm={3}>
              <StyledTypography>
                Title:
                <StyledCapitalizeSpan updated={initialPassengers[index].title !== passenger.title ? "true" : undefined}>
                  {" "}
                  {passenger.title.toLowerCase()}
                </StyledCapitalizeSpan>
              </StyledTypography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <StyledTypography>
                First Name:
                <StyledSpan updated={initialPassengers[index].firstName !== passenger.firstName ? "true" : undefined}>
                  {" "}
                  {passenger.firstName}
                </StyledSpan>
              </StyledTypography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <StyledTypography>
                Last Name:
                <StyledSpan updated={initialPassengers[index].lastName !== passenger.lastName ? "true" : undefined}>
                  {" "}
                  {passenger.lastName}
                </StyledSpan>
              </StyledTypography>
            </Grid>
          </Grid>

          <Grid container spacing={2} style={{ marginBottom: "8px" }}>
            <Grid item xs={12} sm={3}>
              <StyledTypography>
                Gender:
                <StyledCapitalizeSpan
                  updated={initialPassengers[index].gender !== passenger.gender ? "true" : undefined}
                >
                  {" "}
                  {passenger.gender.toLowerCase()}
                </StyledCapitalizeSpan>
              </StyledTypography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <StyledTypography>
                Date of Birth:
                <StyledSpan updated={initialPassengers[index].dob !== passenger.dob ? "true" : undefined}>
                  {" "}
                  {passenger.dob}
                </StyledSpan>
              </StyledTypography>
            </Grid>
          </Grid>
        </ConfirmationWrapper>
      ))}
    </StyledWrapper>
  );
};

export default Confirmation;
