import React, { useEffect, useState } from "react";
import { Accordion, AccordionSummary } from "@mui/material";
import { useForm, useFieldArray } from "react-hook-form";
import PassengerDetails from "../PassengerDetails/PassengerDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { CardWrapper, StyledAccordionDetails, StyledGroupsIcon, StyledTitle, FlexSpaceBetweenWrapper } from "./styles";
import Confirmation from "../Confirmation/Confirmation";
import { PassengersFormData, PassengersManagerProps } from "./types";
import LoadPassengers from "../LoadPassengers/LoadPassengers";
import { PassengersMap } from "../../Pages/App/types";
import { mapToPassengersArray } from "../../utils";
import { PrimaryButton, SecondaryButton } from "../Buttons";

const MAX_NAME_CHANGE = 3;

const PassengersManager: React.FC<PassengersManagerProps> = ({ passengers, handleSave }) => {
  const [isDisabled, setDisabled] = useState(false);
  const [isSubmitted, setSubmit] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [initialPassengers, setInitialPassengers] = useState(passengers);

  const { control, handleSubmit, getValues, setValue } = useForm<PassengersFormData>({
    defaultValues: { passengerArray: [] },
  });
  const { fields } = useFieldArray({ control, name: "passengerArray" });

  const handleSaveChanges = () => {
    setSubmit(true);
  };

  const handleCancel = () => {
    setSubmit(false);
  };

  useEffect(() => {
    setValue("passengerArray", initialPassengers);
  }, [initialPassengers, setValue]);

  const handleLoad = (passengersMap: PassengersMap) => {
    const passengers = mapToPassengersArray(passengersMap);
    setInitialPassengers(passengers);
  };

  return (
    <CardWrapper>
      <Accordion defaultExpanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} id="panel1-header">
          <StyledGroupsIcon />
          <StyledTitle>Passenger Details</StyledTitle>
        </AccordionSummary>
        <StyledAccordionDetails>
          {isSubmitted ? (
            <>
              <Confirmation initialPassengers={initialPassengers} passengers={getValues().passengerArray} />
              <FlexSpaceBetweenWrapper>
                <SecondaryButton onClick={handleCancel}>Back</SecondaryButton>
                <PrimaryButton onClick={handleSave}>Save</PrimaryButton>
              </FlexSpaceBetweenWrapper>
            </>
          ) : (
            <form onSubmit={handleSubmit(handleSaveChanges)}>
              {fields.map((passenger, index) => (
                <PassengerDetails
                  key={passenger.id}
                  index={index}
                  passenger={passenger}
                  maxChanges={MAX_NAME_CHANGE}
                  control={control}
                  setDisabled={setDisabled}
                />
              ))}
              <FlexSpaceBetweenWrapper>
                <PrimaryButton buttonType="submit" isDisabled={isDisabled}>
                  Save Changes
                </PrimaryButton>
                <SecondaryButton onClick={() => setOpenModal(true)}>Load Passenger Details</SecondaryButton>
              </FlexSpaceBetweenWrapper>
            </form>
          )}
        </StyledAccordionDetails>
      </Accordion>
      <LoadPassengers open={openModal} onClose={() => setOpenModal(false)} onSave={handleLoad} />
    </CardWrapper>
  );
};

export default PassengersManager;
