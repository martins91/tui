import { styled } from "@mui/system";
import GroupsIcon from "@mui/icons-material/Groups";
import { AccordionDetails } from "@mui/material";

export const CardWrapper = styled("div")(({ theme }) => ({
  margin: "auto",
  border: "2px solid black",
  borderRadius: "4px",
  width: "90%",
  [theme.breakpoints.up("md")]: {
    width: "80%",
  },
  [theme.breakpoints.up("lg")]: {
    width: "70%",
  },
}));

export const StyledGroupsIcon = styled(GroupsIcon)({
  marginRight: "8px",
  backgroundColor: "hsl(198, 87%, 94%)",
  padding: "2px",
  borderRadius: "50%",
  display: "inline-flex",
});

export const StyledTitle = styled("h4")({
  color: "hsl(248, 69%, 21%)",
  textTransform: "uppercase",
});

export const StyledAccordionDetails = styled(AccordionDetails)({
  backgroundColor: "hsl(198, 87%, 94%)",
});

export const FlexSpaceBetweenWrapper = styled("div")({
  display: "flex",
  justifyContent: "space-between",
});
