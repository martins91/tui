import { Passenger } from "../../Pages/App/types";

export interface PassengersFormData {
  passengerArray: Passenger[];
}

export interface PassengersManagerProps {
  passengers: Passenger[];
  handleSave: () => void;
}
