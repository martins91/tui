# TUI Flight Fulfillment

This project is a React application for editing multiple passenger details. It provides a form interface where users can modify the details of each passenger.

## Features

- Limit the number of changes in the name fields to 3.
- Load saved passenger details via an object into the form
- Double-opt-in confirmation page
- A passenger has the following information:
  - Title (MR or MRS)
  - Gender (MALE or FEMALE)
  - First Name
  - Last Name
  - Date of Birth

## Installation

1. Clone the repository:
   ```
   git clone https://gitlab.com/martins91/tui
   ```

2. Navigate to the project directory:
   ```
   cd tui
   ```

3. Install dependencies:
   ```
   npm install
   ```

## Usage

1. Start the development server:
   ```
   npm start
   ```

2. Open your browser and go to [http://localhost:3000](http://localhost:3000) to view the application.
